<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::redirect('/', '/championships');

    // Teams

    Route::get('/teams', 'TeamController@index')->name('teams.index');
    Route::get('/teams/create', 'TeamController@create')->name('teams.create');
    Route::post('/teams', 'TeamController@store')->name('teams.store');
    Route::get('/teams/{id}', 'TeamController@show')->name('teams.show');
    Route::delete('/teams/{id}', 'TeamController@destroy')->name('teams.destroy');
    Route::put('/teams/{id}', 'TeamController@update')->name('teams.update');
    Route::get('/teams/{id}/edit', 'TeamController@edit')->name('teams.edit');

    // Championships

    Route::get('/championships', 'ChampionshipController@index')->name('championships.index');
    Route::get('/championships/create', 'ChampionshipController@create')->name('championships.create');
    Route::post('/championships', 'ChampionshipController@store')->name('championships.store');
    Route::get('/championships/{id}', 'ChampionshipController@show')->name('championships.show');
    Route::delete('/championships/{id}', 'ChampionshipController@destroy')->name('championships.destroy');
    Route::put('/championships/{id}', 'ChampionshipController@update')->name('championships.update');
    Route::get('/championships/{id}/edit', 'ChampionshipController@edit')->name('championships.edit');

    // --- Championships - Teams

    Route::get('/championships/{id}/teams', 'ChampionshipController@teams')->name('championships.teams');
    Route::get('/championships/{id}/teams/participanting', 'ChampionshipController@teamsParticipanting')->name('championships.teamsParticipanting');
    Route::post('/championships/{id}/teams/participanting', 'ChampionshipController@addTeamParticipanting')->name('championships.addTeamParticipanting');
    Route::delete('/championships/{id}/teams/participanting/{team_championship_id}', 'ChampionshipController@removeTeamParticipanting')->name('championships.removeTeamParticipanting');
    Route::get('/championships/{id}/teams/no-participanting', 'ChampionshipController@teamsNoParticipanting')->name('championships.teamsNoParticipanting');

    // --- Championships - Games

    Route::get('/championships/{id}/games', 'ChampionshipController@games')->name('championships.games');
    Route::post('/championships/{id}/games', 'ChampionshipController@addGame')->name('championships.addGame');
    Route::delete('/championships/{id}/games/{game_id}', 'ChampionshipController@removeGame')->name('championships.removeGame');
    Route::get('/championships/{id}/games/all', 'ChampionshipController@allGames')->name('championships.allGames');

    // Route::get('/championships/{id}/games/all', 'ChampionshipController@gamesParticipanting')->name('championships.teamsParticipanting');
    // Route::post('/championships/{id}/games/participanting', 'ChampionshipController@addTeamParticipanting')->name('championships.addTeamParticipanting');
    // Route::delete('/championships/{id}/games/participanting/{team_championship_id}', 'ChampionshipController@removeTeamParticipanting')->name('championships.removeTeamParticipanting');
    // Route::get('/championships/{id}/games/no-participanting', 'ChampionshipController@teamsNoParticipanting')->name('championships.teamsNoParticipanting');

    // --- Championships - Results
    Route::get('/championships/{id}/results', 'ChampionshipController@results')->name('championships.results');

});
