<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TeamChampionship extends Model
{

    use Notifiable;

    protected $table = 'team_championship';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id',
        'championship_id'
    ];

}
