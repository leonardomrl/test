<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Championship extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The championship that belong to the team.
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class, 'team_championship')->withPivot('id');
    }

    /**
     * The championship that belong to the game.
     */
    public function games()
    {
        return $this->hasMany(Game::class);
    }

}
