<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Team extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The team that belong to the championship.
     */
    public function championships()
    {
        return $this->belongsToMany(Championship::class, 'team_championship')->withPivot('id');
    }

}
