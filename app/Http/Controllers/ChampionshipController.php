<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Championship;
use App\Team;
use App\TeamChampionship;
use App\Game;
class ChampionshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $pesq = Championship::paginate(10);
            return response()->json($pesq);
        }
        return view('championship.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('championship.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:championships|min:3|max:255',
        ]);
        $championship = new Championship();
        $championship->fill($request->input());
        $championship->save();
        return response()->json($championship);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->ajax()){
            $pesq = Championship::findOrFail($id);
            return response()->json($pesq);
        }
        return view('championship.show')->with(['id' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('championship.edit')->with(['id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:255',
        ]);
        $championship = Championship::findOrFail($id);
        $championship->fill($request->input());
        $championship->save();

        return response()->json($championship);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $championship = Championship::findOrFail($id);
        $delete = $championship->delete();
        return response()->json($delete);
    }

    /**
     * Show the screen Teams.
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function teams(Request $request, $id)
    {
        return view('championship.teams')->with(['id' => $id]);
    }

    /**
     *
     * add team that will participate in the competition
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function addTeamParticipanting(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        $championship = Championship::findOrFail($id);
        $team_championship = new TeamChampionship();
        $team_championship->team_id = $request->input('id');
        $team_championship->championship_id = $id;
        $team_championship->save();
        return response()->json($team_championship);
    }

    /**
     * remove team that participate in the competition
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     * @param  int  $team_championship_id
     */
    public function removeTeamParticipanting(Request $request, $id, $team_championship_id)
    {
        $team_championship = TeamChampionship::findOrFail($team_championship_id);
        $delete = $team_championship->delete();
        return response()->json($delete);
    }

    /**
     * Show the teams that are participating in this championship
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function teamsParticipanting(Request $request, $id)
    {
        $pesq = Championship::with('teams')->findOrFail($id);
        return response()->json($pesq);
    }

    /**
     * Show the teams that are not participating in this championship
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function teamsNoParticipanting(Request $request, $id)
    {
        $pesq_teams = Team::get();
        $pesq_championships = Championship::with('teams')->findOrFail($id);

        $teams = [];

        // Know if non-participating teams offer the possibility of adding them to the competition
        foreach ($pesq_teams as $key => $value) {
            $team_id = $value['id'];
            $add = true;
            foreach ($pesq_championships['teams'] as $key2 => $value2) {
                $team_id2 = $value2['id'];
                if ($team_id == $team_id2) {
                    $add = false;
                    break;
                }
            }
            if ($add) {
                array_push($teams, $value);
            }
        }

        return response()->json($teams);
    }

    /**
     * Show the screen Games.
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function games(Request $request, $id)
    {
        return view('championship.games')->with(['id' => $id]);
    }

    /**
     * Show all games that are participating in this championship
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function allGames(Request $request, $id)
    {
        $pesq = Championship::with('games.team_a', 'games.team_b')->findOrFail($id);
        return response()->json($pesq);
    }

    /**
     *
     * add team that will participate in the competition
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function addGame(Request $request, $id)
    {
        $championship = Championship::findOrFail($id);
        $game = new Game();
        $game->fill($request->input());
        $game->championship_id = $id;
        $game->team_a_id = $request->input('team_a_id.id');
        $game->team_b_id = $request->input('team_b_id.id');
        $game->save();
        return response()->json($game);
    }

    /**
     * remove team that participate in the competition
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     * @param  int  $team_championship_id
     */
    public function removeGame(Request $request, $id, $game_id)
    {
        $game = Game::findOrFail($game_id);
        $delete = $game->delete();
        return response()->json($delete);
    }

    /**
     * Show the screen Results.
     *
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function results(Request $request, $id)
    {
        if($request->ajax()){

            $pesq = Championship::with('teams', 'games')->findOrFail($id)->toArray();

            // Insert default values in teams
            foreach ($pesq['teams'] as $key => $value) {

                $team_id = $value['id'];

                $pesq['teams'][$key]['points'] = 0;
                $pesq['teams'][$key]['win'] = 0;
                $pesq['teams'][$key]['draw'] = 0;
                $pesq['teams'][$key]['lose'] = 0;

                // count wins draws and loses
                foreach ($pesq['games'] as $key2 => $value2) {

                    $team_a_id = $value2['team_a_id'];
                    $team_b_id = $value2['team_b_id'];
                    $team_a_goals = $value2['team_a_goals'];
                    $team_b_goals = $value2['team_b_goals'];

                    // check if the game belongs to the team
                    if ($team_id == $team_a_id) {

                        if($team_a_goals > $team_b_goals) {
                            // Win
                            $pesq['teams'][$key]['points'] += 3;
                            $pesq['teams'][$key]['win']++;

                        } else if($team_a_goals < $team_b_goals) {
                            // Win
                            $pesq['teams'][$key]['lose']++;
                        } else {
                            // Win
                            $pesq['teams'][$key]['points']++;
                            $pesq['teams'][$key]['draw']++;
                        }

                    }

                    // check if the game belongs to the team
                    if ($team_id == $team_b_id) {

                        if($team_a_goals < $team_b_goals) {
                            // Win
                            $pesq['teams'][$key]['points'] += 3;
                            $pesq['teams'][$key]['win']++;

                        } else if($team_a_goals > $team_b_goals) {
                            // Win
                            $pesq['teams'][$key]['lose']++;
                        } else {
                            // Win
                            $pesq['teams'][$key]['points']++;
                            $pesq['teams'][$key]['draw']++;
                        }

                    }

                }

            }

            // Order
            usort($pesq['teams'], function ($item1, $item2) {
                return $item2['points'] <=> $item1['points'];
            });

            return response()->json($pesq);
        }
        return view('championship.results')->with(['id' => $id]);
    }

}
