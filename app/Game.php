<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Game extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'championship_id',
        'team_a_id',
        'team_b_id',
        'team_a_goals',
        'team_b_goals',
    ];

    /**
     * The team that belong to the championship.
     */
    public function team_a()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * The team that belong to the championship.
     */
    public function team_b()
    {
        return $this->belongsTo(Team::class);
    }

}
