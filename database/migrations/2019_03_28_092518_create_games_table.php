<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('championship_id');
            $table->integer('team_a_id');
            $table->integer('team_b_id');
            $table->integer('team_a_goals');
            $table->integer('team_b_goals');
            $table->timestamps();
        });

        Schema::table('games', function($table) {
            $table->index('championship_id');
            $table->index('team_a_id');
            $table->index('team_b_id');
            $table->index('team_a_goals');
            $table->index('team_b_goals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
