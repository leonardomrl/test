
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// MultiSelect
import Multiselect from 'vue-multiselect';
Vue.component('multiselect', Multiselect);

// Paginate
Vue.component('pagination', require('laravel-vue-pagination'));

// Widgets
Vue.component('loading', require('./components/widgets/Loading.vue').default);
Vue.component('form-error', require('./components/widgets/FormError.vue').default);

// Team
Vue.component('team-index', require('./components/team/IndexComponent.vue').default);
Vue.component('team-create', require('./components/team/CreateComponent.vue').default);
Vue.component('team-edit', require('./components/team/EditComponent.vue').default);
Vue.component('team-show', require('./components/team/ShowComponent.vue').default);

// Championship
Vue.component('championship-index', require('./components/championship/IndexComponent.vue').default);
Vue.component('championship-create', require('./components/championship/CreateComponent.vue').default);
Vue.component('championship-edit', require('./components/championship/EditComponent.vue').default);
Vue.component('championship-show', require('./components/championship/ShowComponent.vue').default);
Vue.component('championship-navbar', require('./components/championship/NavbarComponent.vue').default);

Vue.component('championship-team-index', require('./components/championship/team/IndexComponent.vue').default);
Vue.component('championship-team-list-all', require('./components/championship/team/ListAllComponent.vue').default);
Vue.component('championship-team-add', require('./components/championship/team/AddComponent.vue').default);

Vue.component('championship-games-index', require('./components/championship/games/IndexComponent.vue').default);
Vue.component('championship-games-list-all', require('./components/championship/games/ListAllComponent.vue').default);
Vue.component('championship-games-add', require('./components/championship/games/AddComponent.vue').default);

Vue.component('championship-results-index', require('./components/championship/results/IndexComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
