@extends('layouts.app')

@section('content')

    <team-create main-route="{{ route('teams.index') }}"></team-create>

@endsection
