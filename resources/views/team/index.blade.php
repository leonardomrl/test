@extends('layouts.app')

@section('content')

    <team-index main-route="{{ route('teams.index') }}"></team-index>

@endsection
