@extends('layouts.app')

@section('content')

    <team-show id="{{ $id }}" main-route="{{ route('teams.index') }}"></team-show>

@endsection
