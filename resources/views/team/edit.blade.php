@extends('layouts.app')

@section('content')

    <team-edit id="{{ $id }}" main-route="{{ route('teams.index') }}"></team-edit>

@endsection
