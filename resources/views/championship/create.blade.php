@extends('layouts.app')

@section('content')

    <championship-create main-route="{{ route('championships.index') }}"></championship-create>

@endsection
