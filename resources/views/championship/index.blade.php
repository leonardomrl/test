@extends('layouts.app')

@section('content')

    <championship-index main-route="{{ route('championships.index') }}"></championship-index>

@endsection
