@extends('layouts.app')

@section('content')

    <championship-team-index id="{{ $id }}" main-route="{{ route('championships.index') }}"></championship-team-index>

@endsection
