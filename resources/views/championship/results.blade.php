@extends('layouts.app')

@section('content')

    <championship-results-index id="{{ $id }}" main-route="{{ route('championships.index') }}"></championship-results-index>

@endsection
