@extends('layouts.app')

@section('content')

    <championship-games-index id="{{ $id }}" main-route="{{ route('championships.index') }}"></championship-games-index>

@endsection
