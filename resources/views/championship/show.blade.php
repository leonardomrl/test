@extends('layouts.app')

@section('content')

    <championship-show id="{{ $id }}" main-route="{{ route('championships.index') }}"></championship-show>

@endsection
