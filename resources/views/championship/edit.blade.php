@extends('layouts.app')

@section('content')

    <championship-edit id="{{ $id }}" main-route="{{ route('championships.index') }}"></championship-edit>

@endsection
